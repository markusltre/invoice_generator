from flask import Flask, render_template
import json
import os
from datetime import date, timedelta
from flask_weasyprint import HTML, CSS, render_pdf

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))

def get_config():
    with open(os.path.join(THIS_FOLDER, 'config.json'), "r") as f:
        return json.load(f)

def get_pdf(path):
    with open(os.path.join(THIS_FOLDER, path)) as f:
        return f.read()

app = Flask(__name__)

customer = {
    "name": "Daniel Anz",
    "email": "info@danielanz.com"
}

billo = {
    "nr": "2020001",
    "date": str(date.today().strftime("%d.%m.%Y")),
    "due_date": str((date.today() + timedelta(days=14)).strftime("%d.%m.%Y")),
    "hint": "Ohne Umsatzsteuer gemäß § 19 Abs. 1 UStG",
    "tax": 0
}

orders = [
        {
            "title": "Softwareentwicklung Web",
            "description": "Option für produktspezifische Preisänderungen im Admin Bereich",
            "price": 80.00,
            "quantity": 1
        }
]
tmp = float(0.0)
number = int(0)
for order in orders:
    order['total'] = float(order.get('price')) * float(order.get('quantity'))
    number += 1
    order['num'] = number
    tmp += float(order.get("total"))

billo['pretax_total'] = tmp;
billo['total_tax'] = tmp * (billo.get('tax')*0.01)
billo['total'] = tmp * (1.0 + (billo.get('tax') * 0.01))

@app.route('/')
def index():
    return render_template("test.html", config=get_config(), customer=customer, bill=billo, orders=orders)

@app.route('/invoice.pdf')
def bill():
    css = CSS(string='@page { size: A4; margin: 0.5cm }')
    response = render_pdf(HTML('http://localhost:5000/'), stylesheets=[css])
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = \
        'inline; filename=%s.pdf' % ('invoice.pdf')
    return response

if __name__ == '__main__':
    app.run(host="localhost", port=5000, debug=True)

